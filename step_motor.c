#include "step_motor.h"

void step_update(u8 cmd){
	switch(cmd){
	case STEP_LEFT:
		step_dir = STEP_LEFT;
		ET0 = 1;
		break;
	case STEP_RIGHT:
		step_dir = STEP_RIGHT;
		ET0 = 1;
		break;
	case STEP_STOP:
		ET0 = 0;
		break;
	default:
		ET0 = 0;
		break;
	}
}

/*	forward() & backward() 前期产物，没用 */

// 正向相序走一遍
void forward(){
	STEP_A=1; STEP_B=0; STEP_C=0; STEP_D=0;
	delay_ms(INTERVAL);
	STEP_A=0; STEP_B=1; STEP_C=0; STEP_D=0;
	delay_ms(INTERVAL);
	STEP_A=0; STEP_B=0; STEP_C=1; STEP_D=0;
	delay_ms(INTERVAL);
	STEP_A=0; STEP_B=0; STEP_C=0; STEP_D=1;
	delay_ms(INTERVAL);
}

// 反向相序走一遍
void backward(){
	STEP_A=0; STEP_B=0; STEP_C=0; STEP_D=1;
	delay_ms(INTERVAL);
	STEP_A=0; STEP_B=0; STEP_C=1; STEP_D=0;
	delay_ms(INTERVAL);
	STEP_A=0; STEP_B=1; STEP_C=0; STEP_D=0;
	delay_ms(INTERVAL);
	STEP_A=1; STEP_B=0; STEP_C=0; STEP_D=0;
	delay_ms(INTERVAL);
}