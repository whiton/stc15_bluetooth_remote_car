#ifndef __MOTOR_H__
#define	__MOTOR_H__

#include "STC15Fxxxx.h"
#include "PCA.h"

#define	MOTOR_ENA1	P10
#define	MOTOR_ENA2	P11
#define MOTOR_ENB1	P12
#define MOTOR_ENB2	P13
#define	PWM_BACK_FORTH	0
#define PWM_LEFT_RIGHT	40
#define PWM_OBLIQUE_HIGH	10
#define PWM_OBLIQUE_LOW		35
#define PWM_ZERO	255	

#define	FORTH		'0'
#define	BACK		'1'
#define	LEFT		'2'
#define	RIGHT		'3'
#define	FORTH_LEFT	'4'
#define	FORTH_RIGHT	'5'
#define	BACK_LEFT	'6'
#define	BACK_RIGHT	'7'
#define STOP		'8'

extern u8 xdata cmd[2];	//16λ����������

void motor_update(u8 cmd);

#endif
