#ifndef	__STEP_MOTOR_H__
#define	__STEP_MOTOR_H__

#include "STC15Fxxxx.h"
#include "delay.h"

#define STEP_A	P00
#define STEP_B	P01
#define STEP_C	P02
#define STEP_D	P03

#define STEP_LEFT	'1'
#define STEP_RIGHT	'0'
#define	STEP_STOP	'2'

#define	INTERVAL	2			//相序切换延时时间，单位ms，决定速度

extern u8 xdata step_dir;
extern u8 xdata cmd[2];

void forward();
void backward();
void step_update(u8 cmd);

#endif