#include "LCD1602.h"

/* �������������� */

void LCD_Delay()
{
	_nop_();
}

void LCD_BusData(u8 dat){
	LCD_B0 = (dat>>0)&0x01;
	LCD_B1 = (dat>>1)&0x01;
	LCD_B2 = (dat>>2)&0x01;
	LCD_B3 = (dat>>3)&0x01;
	LCD_B4 = (dat>>4)&0x01;
	LCD_B5 = (dat>>5)&0x01;
	LCD_B6 = (dat>>6)&0x01;
	LCD_B7 = (dat>>7)&0x01; 
}

void Check_Busy(void)
{
	u16 i;
	for(i=0;i<5000;i++)	{if(!LCD_B7) break;}
}

void Cmd_Send_Init(unsigned char cmd)
{
	LCD_RW=0;
	LCD_BusData(cmd);
	LCD_Delay();
	LCD_EN=1;
 	LCD_Delay();
	LCD_EN=0;
	LCD_BusData(0xff);
}

void Write_CMD(unsigned char cmd)
{
	LCD_RS=0;
	LCD_RW=1;
	LCD_BusData(0xff);
 	LCD_Delay();
	LCD_EN=1;
	Check_Busy();
	LCD_EN=0;
	LCD_RW=0;

	LCD_BusData(cmd);
	LCD_Delay();
	LCD_EN=1;
	LCD_Delay();
	LCD_EN=0;
	LCD_BusData(0xff);
}

void Write_DIS_Data(unsigned char dat)
{
	LCD_RS=0;
	LCD_RW=1;

	LCD_BusData(0xff);
	LCD_Delay();
	LCD_EN=1;
	Check_Busy();
	LCD_EN=0;
	LCD_RW=0;
	LCD_RS=1;

	LCD_BusData(dat);
	LCD_Delay();
	LCD_EN=1;
	LCD_Delay();
	LCD_EN=0;
	LCD_BusData(0xff);
}

void LCD_Init(void)
{
	LCD_EN=0;
	LCD_RS=0;
	LCD_RW=0;
	delay_ms(100);
	Cmd_Send_Init(C_BIT8);
	delay_ms(10);
	Write_CMD(C_L2DOT7);
	delay_ms(6);
	Write_CMD(C_CLEAR);
	Write_CMD(C_CUR_R);
	Write_CMD(C_ON);
	Clear_Line(2);
	Clear_Line(1);
}

void Clear_Line(unsigned char row)
{
	unsigned char i;
	Write_CMD(((row&1)<<6)|0x80);
	for(i=0;i<LineLength;i++)	Write_DIS_Data(' ');
}

void Write_Char(unsigned char row,unsigned char column,unsigned char dat)
{
	Write_CMD((((row&1)<<6)+column)|0x80);
	Write_DIS_Data(dat);
}

void Put_String(unsigned char row,unsigned char column,unsigned char *puts)
{
	Write_CMD((((row&1)<<6)+column)|0x80);
	for(;*puts!=0;puts++)
	{
		Write_DIS_Data(*puts);
		if(++column>=LineLength)	break;
	}
}

void Write_Num(unsigned char row,unsigned char column,long int dat)
{
	unsigned char num[8],i=0,j;
	while(dat)
	{
		num[i]=dat%10+'0';
		dat=dat/10;
		i++;
	}
	j=0;
	if(i==0)
	{
		Write_Char(row,column+i-j,' ');
		Write_Char(row,column,'0');
	}
	i--;
	while(j<=i)
	{
		Write_Char(row,column+i-j,' ');
		Write_Char(row,column+i-j,num[j]);
		j++;
	}
}
