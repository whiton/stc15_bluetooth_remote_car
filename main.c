#include "STC15Fxxxx.h"
#include "step_motor.h"
#include "delay.h"
#include "timer.h"
#include "PCA.h"
#include "USART.h"
#include "pump.h"
#include "motor.h"
#include "LCD1602.h"

/* global variables */
u8 xdata cmd[2];	//16位蓝牙命令字
u8 xdata step_dir;	//步进电机方向

/* local funtion declaration */
void timer_init();
void usart_init();
void pwm_init();
void io_init();

void main(){
	
	EA = 0;
	timer_init();
	pwm_init();
	usart_init();
	io_init();
	LCD_Init();
	EA = 1;

	P04 = 0;	//bluetooth indicator, controlling a led on board, digital signal, referred in usart.c
	
	while(1){
		switch(cmd[0]){
			case 'm':
				motor_update(cmd[1]);
				break;
			case 's':
				step_update(cmd[1]);
				break;
			case 'p':
				pump_update(cmd[1]);
				break;
			default:
				motor_update(STOP);
				step_update(STEP_STOP);
				pump_update(PUMP_CLOSE);
				break;
		}
	}
}

void timer_init(){
	// 定时器初始化
	TIM_InitTypeDef Timer_Init_Param;
	Timer_Init_Param.TIM_Mode = TIM_16BitAutoReload;
	Timer_Init_Param.TIM_Polity = PolityLow;
	Timer_Init_Param.TIM_Interrupt = DISABLE;	//ET=0
	Timer_Init_Param.TIM_ClkSource = TIM_CLOCK_12T;
	Timer_Init_Param.TIM_ClkOut = DISABLE;
	Timer_Init_Param.TIM_Value = 0xF19A;	//2ms
	Timer_Init_Param.TIM_Run = ENABLE;
	Timer_Inilize(Timer0, &Timer_Init_Param);
}

void usart_init(){
	COMx_InitDefine		COMx_InitStructure;
	COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;		//模式,       UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
	COMx_InitStructure.UART_BRT_Use   = BRT_Timer1;				//使用波特率,   BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
	COMx_InitStructure.UART_BaudRate  = 38400ul;				//波特率, 一般 110 ~ 115200
	COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
	COMx_InitStructure.BaudRateDouble = DISABLE;			//波特率加倍, ENABLE或DISABLE
	COMx_InitStructure.UART_Interrupt = ENABLE;				//中断允许,   ENABLE或DISABLE
	COMx_InitStructure.UART_Polity    = PolityHigh;			//中断优先级, PolityLow,PolityHigh
	COMx_InitStructure.UART_P_SW      = UART1_SW_P30_P31;	//切换端口,   UART1_SW_P30_P31,UART1_SW_P36_P37,UART1_SW_P16_P17(必须使用内部时钟)
	COMx_InitStructure.UART_RXD_TXD_Short = DISABLE;		//内部短路RXD与TXD, 做中继, ENABLE,DISABLE
	USART_Configuration(USART1, &COMx_InitStructure);		//初始化串口1 USART1,USART2
}

void pwm_init(){
	// PWM初始化
	PCA_InitTypeDef PCA_InitStructure;
	PCA_InitStructure.PCA_IoUse = PCA_P24_P25_P26_P27;
	PCA_InitStructure.PCA_Clock = PCA_Clock_12T;
	PCA_InitStructure.PCA_Interrupt_Mode = DISABLE;
	PCA_InitStructure.PCA_Polity = PolityLow;
	PCA_InitStructure.PCA_Mode     = PCA_Mode_PWM;		//PCA_Mode_PWM, PCA_Mode_Capture, PCA_Mode_SoftTimer, 	PCA_Mode_HighPulseOutput
	PCA_InitStructure.PCA_PWM_Wide = PCA_PWM_8bit;		//PCA_PWM_8bit, PCA_PWM_7bit, PCA_PWM_6bit
	PCA_Init(PCA_Counter,&PCA_InitStructure);			// 这一大块不确定作用，貌似是必须的
	
	PCA_InitStructure.PCA_Mode     = PCA_Mode_PWM;		//PCA_Mode_PWM, PCA_Mode_Capture, PCA_Mode_SoftTimer, 	PCA_Mode_HighPulseOutput
	PCA_InitStructure.PCA_PWM_Wide = PCA_PWM_8bit;		//PCA_PWM_8bit, PCA_PWM_7bit, PCA_PWM_6bit
	PCA_InitStructure.PCA_Value    = 255 << 8;			//对于PWM,高8位为PWM空周期
	PCA_Init(PCA1,&PCA_InitStructure);	   

	PCA_InitStructure.PCA_Value    = 255 << 8;			//对于PWM,高8位为PWM空周期
	PCA_Init(PCA0,&PCA_InitStructure);	   
}

void io_init(){
	P1M0 = 0;
	P1M0 = 0;
	P2M0 = 0; 
	P2M1 = 0;
	P3M0 = 0;
	P3M1 = 0;
	P4M0 = 0;
	P4M1 = 0;
}
