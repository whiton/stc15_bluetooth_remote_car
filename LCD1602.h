#ifndef __LCD1602_H__
#define __LCD1602_H__

#include "STC15fxxxx.h"
#include "delay.h"

#define LineLength	16

#define LCD_B7 P23
#define LCD_B6 P22
#define LCD_B5 P21
#define LCD_B4 P20
#define LCD_B3 P44
#define LCD_B2 P42
#define LCD_B1 P41
#define LCD_B0 P37

#define LCD_EN P36
#define LCD_RW P35
#define LCD_RS P34

#define C_CLEAR	0x01
#define C_HOME	0x02
#define C_CUR_L	0x04
#define C_RIGHT	0x05
#define C_CUR_R	0x06
#define C_LEFT	0x07
#define C_OFF	0x08
#define C_ON	0x0C
#define C_FLASH	0x0D
#define C_CURSOR	0x0E
#define C_FLASH_ALL	0x0F
#define C_CURSOR_LEFT	0x10
#define C_CURSOR_RIGHT	0x10
#define C_PICTURE_LEFT	0x10
#define C_PICTURE_RIGHT	0x10
#define C_BIT8	0x30
#define C_BIT4	0x20
#define C_L1DOT7	0x30
#define C_L1DOT10	0x34
#define C_L2DOT7	0x38
#define C_4bitL2DOT7	0x28
#define C_CGADDRESS0	0x40
#define C_DDADDRESS0	0x80

void Check_Busy(void);
void Cmd_Send_Init(u8 cmd);
void Write_CMD(u8 cmd);
void Write_DIS_Data(u8 dat);
void LCD_Init(void);
void Clear_Line(u8 row);
void Write_Char(u8 row,u8 column,u8 dat);
void Put_String(u8 row,u8 column,u8 *puts);
void LCD_BusData(u8 dat);
void Write_Num(unsigned char row,unsigned char column,long int dat);


#endif
