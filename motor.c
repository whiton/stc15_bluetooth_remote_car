#include "motor.h"

void motor_update(u8 cmd){
	switch(cmd){
	case FORTH:
		MOTOR_ENA1 = 1;
		MOTOR_ENA2 = 0;
		MOTOR_ENB1 = 1;
		MOTOR_ENB2 = 0;
	   	UpdatePwm(PCA0, PWM_BACK_FORTH);
		UpdatePwm(PCA1, PWM_BACK_FORTH);
		break;
	case BACK:
		MOTOR_ENA1 = 0;
		MOTOR_ENA2 = 1;
		MOTOR_ENB1 = 0;
		MOTOR_ENB2 = 1;
	   	UpdatePwm(PCA0, PWM_BACK_FORTH);
		UpdatePwm(PCA1, PWM_BACK_FORTH);
		break;
	case LEFT:
		MOTOR_ENA1 = 0;
		MOTOR_ENA2 = 1;
		MOTOR_ENB1 = 1;
		MOTOR_ENB2 = 0;
	   	UpdatePwm(PCA0, PWM_LEFT_RIGHT);
		UpdatePwm(PCA1, PWM_LEFT_RIGHT);
		break;
	case RIGHT:
		MOTOR_ENA1 = 1;
		MOTOR_ENA2 = 0;
		MOTOR_ENB1 = 0;
		MOTOR_ENB2 = 1;
	   	UpdatePwm(PCA0, PWM_LEFT_RIGHT);
		UpdatePwm(PCA1, PWM_LEFT_RIGHT);
		break;
	case FORTH_LEFT:
		MOTOR_ENA1 = 1;
		MOTOR_ENA2 = 0;
		MOTOR_ENB1 = 1;
		MOTOR_ENB2 = 0;
	   	UpdatePwm(PCA0, PWM_OBLIQUE_HIGH);	//PCA0 CONTROLS THE RIGHT MOTOR
		UpdatePwm(PCA1, PWM_OBLIQUE_LOW);
		break;
	case FORTH_RIGHT:
		MOTOR_ENA1 = 1;
		MOTOR_ENA2 = 0;
		MOTOR_ENB1 = 1;
		MOTOR_ENB2 = 0;
	   	UpdatePwm(PCA0, PWM_OBLIQUE_LOW);
		UpdatePwm(PCA1, PWM_OBLIQUE_HIGH);
		break;
	case BACK_LEFT:
		MOTOR_ENA1 = 0;
		MOTOR_ENA2 = 1;
		MOTOR_ENB1 = 0;
		MOTOR_ENB2 = 1;
	   	UpdatePwm(PCA0, PWM_OBLIQUE_HIGH);
		UpdatePwm(PCA1, PWM_OBLIQUE_LOW);
		break;
	case BACK_RIGHT:
		MOTOR_ENA1 = 0;
		MOTOR_ENA2 = 1;
		MOTOR_ENB1 = 0;
		MOTOR_ENB2 = 1;
	   	UpdatePwm(PCA0, PWM_OBLIQUE_LOW);
		UpdatePwm(PCA1, PWM_OBLIQUE_HIGH);
		break;
	case STOP:
		MOTOR_ENA1 = 1;
		MOTOR_ENA2 = 0;
		MOTOR_ENB1 = 1;
		MOTOR_ENB2 = 0;
	   	UpdatePwm(PCA0, PWM_ZERO);
		UpdatePwm(PCA1, PWM_ZERO);
		break;
	default:
		MOTOR_ENA1 = 0;
		MOTOR_ENA2 = 0;
		MOTOR_ENB1 = 0;
		MOTOR_ENB2 = 0;
	   	UpdatePwm(PCA0, PWM_ZERO);
		UpdatePwm(PCA1, PWM_ZERO);
		break;
	}
}
