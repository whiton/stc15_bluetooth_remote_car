1 步进电机需要稳定连续执行不能被打断——次高中断优先级
2 泵 toggle mode，稳定高电平即可
3 电机pwm独立驱动，四个IO控制方向
4 优先级：蓝牙串口中断 > 步进电机定时中断

蓝牙命令
	'm'
		#define	FORTH		'0'
		#define	BACK		'1'
		#define	LEFT		'2'
		#define	RIGHT		'3'
		#define	FORTH_LEFT	'4'
		#define	FORTH_RIGHT	'5'
		#define	BACK_LEFT	'6'
		#define	BACK_RIGHT	'7'
		#define STOP		'8'
	's'
		#define STEP_LEFT	'0'
		#define STEP_RIGHT	'1'
		#define	STEP_STOP	'2'
	'p'
		#define PUMP_OPEN	'1'
		#define PUMP_CLOSE	'0'



蓝牙串口中断（）{
	switch（信息高位大分类）{
	case1 
		屏蔽高位；
		电机动作更新（）；
	case2 	
		屏蔽高位；
		泵开关量更新（）；
	case3 
		屏蔽高位；
		步进电机动作更新（）；
	}
}
// 高八位1 2 3

主函数（）{
	初始化（）；
	检测是否从蓝牙收到命令（）
		{
			缓冲区有多少个字符，如果有两个则读取，同时清空缓存区
		}
	解析命令
	更新	
}

电机动作更新（）{
	switch（信息）{
	case前走：
		四个IO调整；
		两路PWM给定量A；
	case后走：
		四个IO调整；
		两路PWM给定量A；
	case左转：
		四个IO调整；
		两路PWM给定量B；
	case右转：
		四个IO调整；
		两路PWM给定量B；
	case左上走：
		四个IO调整；
		两路PWM给定量C；
	case右上走：
		四个IO调整；
		两路PWM给定量C；
	case左下走：
		四个IO调整；
		两路PWM给定量C；
	case右下走：
		四个IO调整；
		两路PWM给定量C；
	case停止：		
		IO调整；
		PWM给空；
	}
}

泵开关量更新（）{
	switch（信息）{
	case动作：
		一个IO调整
	case停止：
		一个IO调整
	}
}

步进电机动作更新（）{
	switch（）{
	case左 	更改方向标志位；
		开定时中断；
	case右	更改方向标志位；
		开定时中断；
	case停	关中断；
	}
}

步进电机定时中断（）{
	if（标志位）{
		正转
	} else {
		反转
	}
}
